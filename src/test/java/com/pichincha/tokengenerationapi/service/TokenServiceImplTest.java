package com.pichincha.tokengenerationapi.service;

import com.pichincha.tokengenerationapi.configuration.PropertiesConfiguration;
import com.pichincha.tokengenerationapi.service.dto.TokenDto;
import com.pichincha.tokengenerationapi.service.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TokenServiceImplTest {
    @Mock
    private PropertiesConfiguration propertiesConfiguration;
    @InjectMocks
    private TokenServiceImpl tokenService;

    private UserDto userDto;

    @BeforeEach
    void setUp() {
        userDto = new UserDto("username", "password");
    }

    @Test
    @DisplayName("Test method to generate a token")
    void givenUserWhenGenerateTokenThenTokenIsGenerated() {
        //Set up the mock
        when(propertiesConfiguration.getJwtSigningKey()).thenReturn("413F4428472B4B6250655368566D5970337336763979244226452948404D6351");

        //Execute the service call
        TokenDto returnedToken = tokenService.generateToken(userDto);

        //Assert the response
        assertNotNull(returnedToken, "The generated token should not be null");
    }
}
package com.pichincha.tokengenerationapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pichincha.tokengenerationapi.service.TokenServiceImpl;
import com.pichincha.tokengenerationapi.service.dto.TokenDto;
import com.pichincha.tokengenerationapi.service.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith({SpringExtension.class})
@SpringBootTest
@AutoConfigureMockMvc
class TokenControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TokenServiceImpl tokenService;

    private String jwt;

    @BeforeEach
    void setUp() {
        jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImlhdCI6MTcwNDQ3NDQzMCwiZXhwIjoxNzA0NDc1ODcwfQ.mbSpLr2iqYEYbz0fDlcjqVj12LGUMdTsjcjElgcAygQ";
    }

    @Test
    void givenUserWhenGenerateTokenThenTokenIsGenerated() throws Exception {
        //Set up the mock
        UserDto userDto = new UserDto("username", "password");
        TokenDto tokenDto = new TokenDto(jwt);
        when(tokenService.generateToken(any(UserDto.class))).thenReturn(tokenDto);

        //Execute the service call
        mockMvc.perform(post("/token/generate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDto)))

                // Validate the response code and content type
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                // Validate the returned fields
                .andExpect(jsonPath("$.jwt", is(jwt)));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
package com.pichincha.tokengenerationapi.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
@ConfigurationProperties
public class PropertiesConfiguration {
    @Value("${token.signing.key}")
    private String jwtSigningKey;
}

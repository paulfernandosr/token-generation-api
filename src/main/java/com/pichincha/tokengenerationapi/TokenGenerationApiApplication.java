package com.pichincha.tokengenerationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokenGenerationApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(TokenGenerationApiApplication.class, args);
	}
}

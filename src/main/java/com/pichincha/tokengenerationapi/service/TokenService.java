package com.pichincha.tokengenerationapi.service;

import com.pichincha.tokengenerationapi.service.dto.TokenDto;
import com.pichincha.tokengenerationapi.service.dto.UserDto;

public interface TokenService {
    TokenDto generateToken(UserDto user);
}

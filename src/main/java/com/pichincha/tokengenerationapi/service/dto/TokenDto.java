package com.pichincha.tokengenerationapi.service.dto;

public record TokenDto(String jwt) {
}

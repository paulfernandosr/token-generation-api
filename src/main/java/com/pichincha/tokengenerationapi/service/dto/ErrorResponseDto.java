package com.pichincha.tokengenerationapi.service.dto;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public record ErrorResponseDto(
        String message,
        HttpStatus status,
        LocalDateTime timestamp
) {
}

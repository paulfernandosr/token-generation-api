package com.pichincha.tokengenerationapi.service.dto;

public record UserDto(String username, String password) {
}

package com.pichincha.tokengenerationapi.service;

import com.pichincha.tokengenerationapi.configuration.PropertiesConfiguration;
import com.pichincha.tokengenerationapi.service.dto.TokenDto;
import com.pichincha.tokengenerationapi.service.dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {
    private final PropertiesConfiguration propertiesConfiguration;

    @Override
    public TokenDto generateToken(UserDto user) {
        log.info("generateToken method initialized with username: " + user.username());
        return generateToken(new HashMap<>(), user);
    }

    private TokenDto generateToken(Map<String, Object> extraClaims, UserDto user) {
        String jwt = Jwts.builder()
                .setClaims(extraClaims)
                .setSubject(user.username())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
        log.info("generateToken method successfully completed with token: " + jwt);
        return new TokenDto(jwt);
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(propertiesConfiguration.getJwtSigningKey());
        return Keys.hmacShaKeyFor(keyBytes);
    }
}

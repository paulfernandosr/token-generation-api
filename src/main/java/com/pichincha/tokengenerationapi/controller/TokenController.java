package com.pichincha.tokengenerationapi.controller;

import com.pichincha.tokengenerationapi.service.TokenService;
import com.pichincha.tokengenerationapi.service.dto.TokenDto;
import com.pichincha.tokengenerationapi.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/token")
public class TokenController {
    private final TokenService tokenService;

    @PostMapping("/generate")
    public ResponseEntity<TokenDto> generateToken(@RequestBody UserDto userDto) {
        TokenDto token = tokenService.generateToken(userDto);
        return ResponseEntity.ok(token);
    }
}
